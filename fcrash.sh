#!/usr/bin/env bash
#fcrash.sh Don't just ask for your CPU or RAM back. Take it by force.
#Copyright (C) 2018  z0x gitlab.com/z0x
#
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
set -e
clear

pause_tabs=0 #fakebool
tabs_to_skip=0 # set the default number of "always on" tabs. Handy if you want to keep your pinned tabs.

if [ ! -z "$1" ] ; then
    argz1=$(echo -e "$1" | tr -dc '[:alpha:]')
    if [ ! -z "$argz1" ] ; then
        pause_tabs=1
    else
         argz1=$(echo -e "$1" | tr -dc '[:digit:]')
         if [ ! -z "$argz1" ] ; then
            tabs_to_skip=$(( tabs_to_skip+argz1 ))
         fi
    fi

    if [ ! -z "$2" ] ; then
        argz2="$(echo -e "$2" | tr -dc '[:digit:]')"
        if [ ! -z "$argz2" ] ; then
            tabs_to_skip=$(( tabs_to_skip+argz2 ))
        fi
    fi
fi

pids_to_skip=$(( tabs_to_skip + 1 ))
echo -e "##"
echo -e "# fcrash v1.1 (C) gitlab.com/z0x\n# Released under GPLv2"
echo -e "##\n"
echo -e "Finding firefox pids..."
ff_arr=$( ps ax | grep "[f]irefox" )                                                                  # get ps output and filter those that contain "firefox"
pid_arr=$( echo "${ff_arr[@]}" | awk -v skip="$pids_to_skip" 'BEGIN {FS=" "} NR>skip {print $1}' )  # grab every pid from that list, begining with the second line
if [ ! -z "$pid_arr" ] ; then                                                                       # make sure you actually have PIDs to kill
    if [ "$pause_tabs" -eq 1 ] ; then
                echo -n "Pausing all "
                if [ "$tabs_to_skip" -gt 0 ] ; then
                    echo -n "but ($tabs_to_skip) "
                fi
                echo -n "Firefox Tabs."
                echo -e "\nHit return to resume..."
                echo "${pid_arr[@]}" | xargs kill -STOP
                read
                echo -e "Unpausing Firefox Tabs..."
                echo "${pid_arr[@]}" | xargs kill -CONT
                echo "Finished. Goodbye."
    else
        echo -n "Killing all"
        if [ "$tabs_to_skip" -gt 0 ] ; then
            echo -n " but ($tabs_to_skip) "
        fi
        echo -n " Firefox tabs..."
        echo "${pid_arr[@]}" | xargs kill  -s 9                                                     # then do the unthinkable.
        echo -e "\nIt is done."
    fi
else
    echo -e "All Firefox tabs already dead."
fi

