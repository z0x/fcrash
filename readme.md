# fcrash.sh

Don't just ask for your RAM and CPU back. Take them by force.
___

#### About

`fcrash.sh` is a script that kills all open firefox tabs to free
up RAM, or can temporarily `stop` all tab processes, freeing up CPU cycles, without closing any windows.

Or rather, `fcrash.sh` messes with all but the smallest firefox pid.
This generally turns out to be the "main" firefox process.

If you can figure out how to get a tab pid < the main firefox process,
let me know.

![example0](media/example0.gif)

#### Usage

`./fcrash.sh [N]` - Crash Firefox tabs, optionally skipping the fist `N` tabs (by pid)

`./fcrash.sh [anything alphabetical] [N]` - pause and unpause Firefox tab processes,
 optionally keeping the first `N` tabs unpaused.

#### Examples
 * `./fcrash.sh 1` - Crash all but 1 Firefox tab

 * `./fcrash.sh qwerty 2` - Pause all but 2 Firefox tabs

 * `./fcrash.sh thesearethevoyagesofthestarshipenterprise` - Pause all Firefox tabs
    * any alphabetical string  passed as the first argument will enter 'pause mode'

#### Notice
Everything in the media folder and anything not covered by GPLv2 is public domain.
